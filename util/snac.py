# vim: ts=2:sw=2:sts=0:noet:nu
"""
These classes load a whole directory (genome) of snac results for convenient
access/processing.
"""
from sys import stderr
from os.path import splitext,isfile,join as path_join
from os import listdir
from bisect import bisect
from collections import namedtuple
from random import choice,randint
import intervaltree as it

_MIN_COMBINABLE_DEPTH = 60

CAC = namedtuple("CAC",['positions','counts'])

_NON_AUTOSOME_KEYS = {'X':23,'Y':24,'M':25}
def _chromoKey( s ):
	v = s[3:].upper() if s.lower().startswith("chr") else s.upper()
	return int(v) if v.isdigit() else _NON_AUTOSOME_KEYS[v[0]]


class Region:
	def __init__( self, *args ):
		if len(args) >= 3:
			self.chrom =     args[0]
			self.start = int(args[1])
			self.end   = int(args[2])
		else:
			raise RuntimeError( "Expected 3 args: chrom, start, end " )
	def __str__( self ):
		return f"{self.chrom}\t{self.start}\t{self.end}"


class ROI:
	"""
	This is essentially a facade around the pypi.org intervaltree package
	that simplifies (?) building and querying IntervalTrees for annotation
	of each chromosome in a genome.
	"""
	def __init__( self, fp ):
		"""
		Convert the BED3 content in fp into a dict mapping chromosome identifiers
		to IntervalTree's containing mappable intervals.
		"""
		contigs = dict()
		for line in fp:
			# Skip standard BED file header and any line that has a standard
			# comment prefix.
			if line.startswith('track') or line.startswith("#"):
				continue
			f = line.rstrip().split('\t')
			start = int(f[1])
			end   = int(f[2])
			assert start <= end
			contigs.setdefault( f[0], list() ).append( it.Interval( start, end ) )
		self.trees = dict( ( k, it.IntervalTree(l) ) for k,l in contigs.items() )

	def covers( self, chrom, pos ) -> bool:
		try:
			tree = self.trees[chrom]
		except KeyError:
			return False
		return len(tree[pos]) > 0

	def as_list( self ):
		l = []
		for k in sorted(self.trees.keys(),key=_chromoKey):
			for i in sorted(self.trees[k],key=lambda i:i.begin):
				l.append( Region( k, *i ) )
		return l


class AlleleCounts:
	def __init__( self, fields ):
		self.pos   = int(fields[0])
		self.depth = int(fields[1])
		n = int(fields[2])
		self.alleles = list(zip(
			[ int(fields[i]) for i in range(3,3+2*n,2) ],
			[     fields[i]  for i in range(4,4+2*n,2) ]))
		assert all(p[1] in "ACGT" for p in self.alleles)
		self.counts = [ pair[0] for pair in self.alleles ] #sorted(..., reversed=True )
		# snac orders alleles descending by count; verify this!
		assert all( self.counts[i] <= self.counts[i-1] for i in range(1,len(self.counts)) )

	def __str__( self ):
		return '\t'.join( f"{c}:{a}" for c,a in self.alleles )

	def allele_ratio( self, top=2 ) -> float:
		if len(self.alleles) == 1:
			return 1.0
		else:
			return self.counts[0] / sum(self.counts[:top])

	@property
	def biallelic_depth( self ):
		"""
		Depth considering only the two most abundant alleles. This
		implicitly assumes any additional alleles are read errors.
		"""
		return sum( self.counts[:2] )


class Genome:
	"""
	Holds pairs of parallel lists of integer positions and AlleleCounts
	objects in a dict keyed on chromosome name. AlleleCounts at index i
	describe the (SNV) alleles at position i.
	"""
	@staticmethod
	def _loadChromo( fp, mindepth=1 ):
		"""
		snac operates on one contig at a time, so its output files correspond
		to individual chromosomes. This loads one of them.
		"""
		positions = []
		counts = []
		for line in fp:
			if not line.startswith('#'):
				f = line.rstrip().split('\t')
				if int(f[1]) >= mindepth:
					positions.append(       int(f[0])  )
					counts.append( AlleleCounts(f    ) )
		return CAC(positions,counts)

	def __init__( self, directory, mindepth=1 ):
		self.distro = dict()
		chromosomes = []
		for filename in listdir(directory):
			path = path_join( directory, filename )
			if filename.endswith("tab") and isfile( path ):
				donor,chrom = splitext(filename)[0].split('_') # e.g. GE0325_chr12.tab
				with open(path) as fp:
					poco = Genome._loadChromo(fp,mindepth)
					# Could be empty no positions had sufficient depth, so...
					if len(poco.positions) > 0:
						for i,ac in enumerate(poco.counts):
							self.distro.setdefault( ac.depth, [] ).append( (chrom,i) )
						chromosomes.append( (chrom, poco) )
		self.data = dict( chromosomes )
		self.maxdepth = max( self.distro.keys() )
		assert all( len(p[0]) > 0 for p in self.data.values() ),\
			"Should not be any chromosomes without positions in self.data"

	def __str__( self ):
		return '\n'.join( f"{k}:{len(self.data[k])}" for k in self.data.keys() )

	def extract( self, rois:"list of (chrom,start,end)" ):
		"""
		yield (chromo,pos,AlleleCounts) triples for all positions covered by
		the rois.
		"""
		for roi in rois:
			try:
				chromosome = self.data[ roi.chrom ]
			except KeyError:
				print( f"Genome missing {roi.chrom}", file=stderr )
				continue
			i = bisect(chromosome.positions,roi.start-1)
			while chromosome.positions[i] <= roi.end:
				yield (roi.chrom,chromosome.positions[i],chromosome.counts[i])
				i += 1

	def random( self, depth=None ) -> (str,int,AlleleCounts):
		"""
		Return either an entirely random position's allele counts or, if depth
		is supplied, a random choice from all positions with that read depth.
		If the depth constraint can't be satisfied exactly, it is satisfied
		approximately. This function ALWAYS returns a valid triple.
		"""
		chrom = None
		i = None
		if depth:
			# Return a random draw with...
			# 1. the same depth _if possible_
			# 2. a larger depth if not, and
			# 3. any depth otherwise.
			if depth >= _MIN_COMBINABLE_DEPTH:
				depth = choice( [ k for k in self.distro.keys() if k >= _MIN_COMBINABLE_DEPTH ] )
				chrom,i = choice( self.distro[depth] )
			else:
				while chrom is None and depth <= self.maxdepth:
					try:
						chrom,i = choice( self.distro[depth] )
					except KeyError:
						depth += 1
		if chrom: # because preceding clause succeeded...
			positions,counts = self.data[chrom]
		else: # Preceding clause was skipped OR failed.
			# Because we may need this again cache the list of keys (i.e.
			# chromosome names) in an attribute.
			if not hasattr(self,"chromosomes"):
				setattr(self,"chromosomes",list(self.data.keys()))
			# Choose an entirely random chromosome.
			chrom = choice(self.chromosomes)
			positions,counts = self.data[chrom] 
			assert len(positions) > 0
			# ...then a random index into the list of read-covered positions.
			i = randint(0,len(positions)-1)
		return (chrom,positions[i],counts[i])


if __name__=="__main__":
	from sys import argv,stdin,exit
	sample_directory = argv[1]
	K = int(argv[2]) if len(argv) > 2 else 1

	deletions = ROI( stdin )
	# TODO: IntervalTree is 0-based, half-open, but input regions are not.
	# Reconcile these.

	# Load the snac data for all chromosomes from one donor.
	g = Genome( sample_directory, mindepth=2 )
	# The Genome class builds lists of the coordinates of all SNVs
	# at all observed read depths as part of Genome loading.
	# We need the distribution of depths in the deletion regions.
	deletion_distro = dict()
	for triple in g.extract( deletions.as_list() ):
		print( "D", triple[2].depth, "{:.2f}".format(triple[2].allele_ratio()), sep="\t" )
		deletion_distro.setdefault( triple[2].depth, [0,] )[0] += 1

	# Now randomly sample the Genome, excluding areas in deletions,
	# such that the distribution of depths is as close to that in
	# deletions as possible.

	for k in range(K):
		for depth,l in deletion_distro.items():
			for i in range(l[0]):
				while True:
					rac = g.random( depth )
					if deletions.covers( *rac[:2] ):
						continue
					print( f"N{k}", rac[2].depth, "{:.2f}".format(rac[2].allele_ratio()), sep="\t" )
					break

