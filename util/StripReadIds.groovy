
/**
	* Postprocess snac output files to either strip or "compress" the read
	* identifiers which account for most of these files' size.
	*/

def KEYWORD_ARG = "discard";

if( args.size() < 1 ) {
	println """\
groovy StripReadIds.groovy <snac file> [ { ${KEYWORD_ARG} | <filename> } ]

In all cases the altered snac file is emitted on stdout, either
with read identifiers stripped out or replaced by ordinals.

If a second argument (other than '${KEYWORD_ARG}') is provided it is treated as a
filename, and the original read identifiers are written to it in ordinal
sequence. If and only if the 2nd argument is exactly '${KEYWORD_ARG}', the columns
with read identifiers are removed, and no record of them is preserved.

Note that if no 2nd argument is given, the read identifiers are still
replaced by ordinals (to "compress" the file), and the original read
identifiers are simply lost."""
	System.exit(-1)
}

def reads_filename = args.size() < 2 || args[1] == KEYWORD_ARG ? "/dev/null" : args[1]
def reads_file = new File( reads_filename )

if( reads_file.isFile() /* /dev/null is not a file. */ ) {
	throw new RuntimeException("${reads_filename} exists. Not clobbering!")
}

def strip( InputStream inp, boolean emit_columns, PrintWriter w ) {

	def reads = new HashMap<String,int>()
	inp.eachLine( line -> {

		if( ! line.startsWith('#') ) {

			// 11418	60	3	35	T	24	C	1	G
			def fields = line.split('\t')
			def na = Integer.parseInt( fields[2] )
			def first_read_field = 3 + 2*na

			if( na > 0 ) {

				String columns = null; // unless... 

				if( emit_columns ) {

					// Convert read identifiers into hex numbers, preserving columns.

					columns = (0..<na).collect( ai -> { 
						fields[ first_read_field + ai ].split(',').collect( rid -> {
							Integer ordinal = reads.get( rid )

							if( ordinal == null ) {

								// This is the first time we've seen rid, so emit it into
								// the output list and map it to the number of reads that
								// preceded it.

								ordinal = reads.size()
								reads.put( rid, ordinal )
								w.println( rid )
							}

							Integer.toHexString( ordinal )
						}).join(",")
					}).join("\t")
				}

				// Emit a line identical to the input line except that sequencing-
				// machine read identifiers have been replaced by their hexadecimal
				// ordinals...or eliminated altogether!

				print "${fields[0]}\t${fields[1]}\t${fields[2]}"
				for(int i = 0; i < na; i++ ) {
					print "\t${fields[3+2*i]}\t${fields[4+2*i]}"
				}
				if( columns != null ) {
					println "\t${columns}"
				} else {
					println "" // just to end the line!
				}

			} else {
				println line // It's a degenerate line, but leave it unscathed.
			}

		} else {
			println line // It's a comment line.
		}
	})
}


def discard_columns = args.size() == 2 && args[1] == KEYWORD_ARG

// We always have an output file (possibly /dev/null), but the input
// source depends on whether or not first argument is a regular file.

reads_file.withPrintWriter( w -> {
	def input = new File( args[0] );
	if( input.isFile() ) {
		input.withInputStream {
			strip( it, ! discard_columns, w )
		}
	} else {
		strip( System.in, ! discard_columns, w );
	}
})

