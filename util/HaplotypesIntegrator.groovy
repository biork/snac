
import groovy.transform.MapConstructor
import java.util.regex.Matcher;

/**
	* Appends alleles from a genome's two haplotypes (defined in a pair
	* of FASTA files) as new rightmost columns in a snac output file.
	*
	* <ol>
	* <li>The haplotype files are assumed to have paths differing in a single
	*		character which should be in {1,2} according to haplotype.
	* <li>
	* </ol>
	*/
@MapConstructor
class HaplotypesIntegrator {

	String snacFile          // required
	String fastaTemplatePath // required
	List   seqName = null    // optional

	/**
		* Pull the sequence name out of the first FASTA header found in the
		* given FASTA file.
		* The sequence name is assumed to be the first string of strictly
		* alphanumeric characters possibly with a chr prefix, following the
		* greater-than character; it stops at whitespace or any punctuation.
		*/
	private String firstSequenceName( File fastaFile ) {
		String name = null
		fastaFile.eachLine {
			if( it.startsWith(">") ) {
				Matcher m = it =~ /^>(?<seq>(?i:chr)?[0-9A-Za-z]+)/
				if( m.find() ) {
					name = m.group("seq")
				} else {
					throw new RuntimeException("header ${it.trim()} does not match sequence name regex")
				}
			}
		}
		if( name == null ) {
			throw new RuntimeException("header not found in ${fastaFile}")
		}
		return name
	}


	File exec( File output = null ) {

		List fastaFiles = [1,2].collect { new File( String.format( fastaTemplatePath, it ) ) }

		if( fastaFiles.any { ! it.exists() } ) {
			throw new RuntimeException("one or both of \"${fastaTemplatePath}\" missing or unreadable")
		}

		List hapFile = [ // deleted before return
			File.createTempFile( "hap1", ".txt" ),
			File.createTempFile( "hap2", ".txt" ) ]

		if( seqName == null ) {
			seqName = [
				firstSequenceName( fastaFiles[0] ),
				firstSequenceName( fastaFiles[1] ) ]
		}

		File result   = File.createTempFile( "snacWithHaplotypes", ".tab" ) // Caller's responsibility

		String script = """

			bedtools getfasta \
				-fi "${fastaFiles[0]}" \
				-bed <(awk -v OFS=\$'\\t' '!/^#/ {print "${seqName[0]}",\$1-1,\$1}' ${snacFile}) \
				| grep -v '^>' \
				| tr '[:lower:]' '[:upper:]' > ${hapFile[0]}
			bedtools getfasta \
				-fi "${fastaFiles[1]}" \
				-bed <(awk -v OFS=\$'\\t' '!/^#/ {print "${seqName[1]}",\$1-1,\$1}' ${snacFile}) \
				| grep -v '^>' \
				| tr '[:lower:]' '[:upper:]' > ${hapFile[1]}

			paste <(grep -v '^#' ${snacFile}) ${hapFile[0]} ${hapFile[1]} > ${result}"""

		// TODO: for debugging: print script.replaceAll('\t',' ')

		Process p = "bash".execute();
		(p << script).close()
		p.waitFor()

		hapFile.each { it.delete() }

		return result; // a tmp file path that is caller's responsibility
	}
}
