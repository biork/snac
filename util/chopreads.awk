
# Chop the QNAME columns off, leaving only pos,depth,allele_counts...

!/^#/ {
 	printf("%s",$1)
	for(i=2;i<=3+2*$3;i++) {
		printf("\t%s",$i)
	}
	printf("\n")
}
