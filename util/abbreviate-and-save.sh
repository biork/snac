
# Run from snac's root output dir.

STRIPPER="${HOME}/projects/snac/util/StripReadIds.groovy"

find K* GE* -name "*_chr*.tab" | while read P; do
	DESTDIR="../stripped/$(dirname ${P})";
	OUTPUT_ID="${DESTDIR}/$(basename ${P%.tab})_ids.txt"
	OUTPUT_COUNTS="${DESTDIR}/$(basename ${P%.tab}_counts.tab)"
	#echo "${STRIPPER} ${P} ${OUTPUT_ID} > ${OUTPUT_COUNTS}";continue
	[ -d ${DESTDIR} ] || mkdir -p ${DESTDIR}
	groovy ${STRIPPER} ${P} ${OUTPUT_ID} > ${OUTPUT_COUNTS}
	gzip ${OUTPUT_ID}
	bgzip ${OUTPUT_COUNTS}
done
