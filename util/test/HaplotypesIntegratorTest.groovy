

def job = new HaplotypesIntegrator(
	snacFile:args[0],
	fastaTemplatePath:args[1],
	seqName: args.size() > 3 ? [ args[2], args[3] ] : null )
def result = job.exec()
println result
