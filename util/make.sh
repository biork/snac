#!/usr/bin/bash

# Because the Strip... and Unstrip... utilities are only run on large sets of
# large files, we definitely don't want to add compilation time to the total
# execution time by running these as groovy scripts. Precompile!

MANIFEST='Manifest.txt'
CONTENT='classes'

while read MAIN JAR; do
	rm -rf ${JAR} 
	cat > ${MANIFEST}  <<EOF
Main-Class: ${MAIN}
Class-Path: tmp-jar/groovy.jar
EOF
	groovyc -d ${CONTENT} ${MAIN}.groovy 
	jar cfm ${JAR} ${MANIFEST} -C ${CONTENT} .
	rm -rf ${CONTENT} ${MANIFEST}
done <<EOF
StripReadIds strip.jar
UnstripReadIds unstrip.jar
EOF

