
/**
	* Identify phasesets and analyze the single-SNV pileups output from snac
	* optionally integrating haplotypes at interrogated positions.
	*
	* Though it's possible, snac does not implement online phasing.¹ The
	* snac program treats all alignment files as single-end and reads (or
	* read _ends_) covering SNVs are merely identified and counted.
	* However, because identifiers of reads supporting allele counts are
	* output for each position, a post-process can identify and reconcile
	* phaseable SNVs. This script implements such a postprocess.
	*
	* This was motivated by ultimate goal of assigning a dominant haplotype
	* identifier in {0,1,2} to every SNV that can then be smoothed with an
	* HMM.
	*
	* This script merely incorporates haplotype data by ordering the otherwise
	* arbitrarily-ordered alleles at each SNV according to putative haplotype:
	* the 1st allele is haplotype 1 and the 2nd allele is haplotype 2. Other
	* observed alleles remain arbitrarily-ordered.
	*
	* 1. identifies all phasesets
	* 2. identifies ambiguous positions and likely problematic reads
	*		covering them
	* 3. optionally emits a BED file containing both single-SNV pileups
	*		and fully-phased intervals.
	* 4. optionally integrates alleles from haplotypes into result
	*
	* Integrate a snac allele counts output file with the corresponding
	* donor's putative haplotypes.
	* 
	* This script is motivated by several goals:
	* 1. enable downstream _multiscale_ haplotype analysis
	* 2. relate allele abundance to (putative) haplotypes
	* 3. improve quantification by removing bad reads
	* 4. explicitly identify phaseable regions (reads covering multiple SNVs)
	*
	* The snac utility merely counts reads at a list of hetSNV positions and
	* outputs:
	* 0. position (1-based)
	* 1. total depth
	* 2. count of observed alleles [1,4]
	* 3. count of allele1
	* 4. allele1 base
	* 5. count of allele2
	* 6. allele2 base
	* ...etc for alleles 3 and 4
	*
	* It also optionally outputs read identifiers ("QNAMES") associated with
	* each allele at each position in subsequent columns, and the order of
	* read columns corresponds to the order of alleles in earlier columns.
	*
	* 
// 2. Reads that cover multiple SNVs will be represented as intervals 
//		assigned to a single haplotype in BED-format output and their
//		counts deducted from individual SNVs.
// 3. Reads that do not span SNVs will be "piled up" at single basepair
//		"intervals"
	* <ol>
	* <li>FASTA haplotype file paths must differ by single char in {1,2}.
	* <li>
	* </ol>
	*/

//import java.util.regex.Matcher;
import java.time.Instant;
import java.nio.file.Files;



/**
	* Encapsulates the list of SNVs ([position,allele] pairs) covered by a
	* single read.
	*
	* All positions in a PhaseSet are 1-based.
	*/
class PhaseSet {

	int read_index
	List snvs // 1-based

	PhaseSet( int read_index ) {
		this.read_index = read_index
		this.snvs = []
	}

	void leftShift( List pos_allele ) {
		assert snvs.every { it[0] != pos_allele[0] } : "phase sets should never contain redundant positions (${read_index}:${pos_allele})"
		snvs << pos_allele
	}

	/**
		* A PhaseSet's signature is the ordered sequence of sequence positions
		* it covers.
		*/
	List signature() {
		return snvs.collect {it[0]}
	}

	String haplotype() {
		return snvs.collect {it[1]}.join("")
	}

	PhaseSet finish() {
		snvs.sort( true, (l,r) -> { l[0] <=> r[0] } )
		return this
	}

	int min() {
		// Following assertion was being triggered by snvs at which overlapping
		// paired ends disagreed. The snac utility now discards those.
		assert (1 ..<snvs.size).every { snvs[it-1][0] < snvs[it][0] }
		return snvs[ 0][0]
	}

	int max() {
		assert (1 ..<snvs.size).every { snvs[it-1][0] < snvs[it][0] }
		return snvs[-1][0]
	}
}


/**
	* An interval defined by results of counting reads.
	* There exist 2 possibilities:
	* 1. single-SNV "intervals" defined by unphaseable hetSNVs for which we
	*		have allele counts
	* 2. multi-SNV intervals defined by a set of SNVs covered by at least one
	*		read, and therefore phaseable.
	* A goal of this utility is assignment of "dominant haplotype" to every
	* interval. This
	* BED format: 0-based, half-open
	*/
class Interval implements Comparable<Interval> {

	int  start
	int  end

	// Either the following two...
	List number
	List allele
	// ...or this field are used, not both.
	List phased

	/**
		* Create a single-SNV "interval".
		*/
	Interval( int n_alleles, List columns ) {

		this.start = Integer.parseInt( columns[0] )
		this.end   = start
		this.number = (0 ..<n_alleles).collect { Integer.parseInt( columns[3+2*it] ) }
		this.allele = (0 ..<n_alleles).collect {                   columns[4+2*it]   }
		this.phased = null
	}

	Interval( PhaseSet pset ) {
		this.phased = []
		this.phased << pset.finish()
		this.start = pset.min()
		this.end   = pset.max()
	}

	boolean isSingleSNV() {
		return phased == null || phased.size == 0
	}

	/**
		* Returns the total number of reads supporting this interval.
		*
		* If it's a single-SNV interval they can be unambiguously interpreted as
		* depth at the SNV; obviously interpretation at a multi-SNV interval is
		* more complex.
		*/
	int getDepth() {
		return isSingleSNV() ? number.sum() : phased.size
	}

	/**
		* Add right-hand-side's PhaseSets to this Interval, taking
		* ownership of them.
		*/
	public Interval leftShift( Interval rhs ) {
		rhs.phased.each { this.phased << it }
		return this
	}

	/**
		* Sort 1st by start and 2nd by end coordinates.
		*/
	public int compareTo( Interval rhs ) {
		return this.start == rhs.start ? this.end <=> rhs.end : this.start <=> rhs.start
	}

	/**
		* Deduct 1 from the count of alleles at the given index and
		* return the allele with its position.
		*
		* The coordinate is reconverted to 1-based
		*/
	List remove( int index ) {
		assert phased == null && number != null && number.size == allele.size : "this should only be called on single-SNV Intervals"
		char allele = this.allele[ index ]
		number[ index ] -= 1
		assert number[ index ] >= 0: "allele counts should remain non-negative"
		return [ start, allele ]
	}

	/**
		* Summarize the allele counts of a single-SNV interval.
		*/
	private String alleleSummary() {
		return "${number.size}:${number.join(",")}:${allele.join("")}"
	}

	/**
		* Group PhaseSets by the positions they cover and count all distinct
		* haplotypes of each PhaseSet group.
		*/
	private String phaseSetSummary() {

		Map psg = [:]

		// Each set of phased positions (e.g. signature) will have one or
		// more reads covering it, each with a haplotype.

		phased.each {
			psg.get( it.signature(), [] ) << [ it.read_index, it.haplotype() ]
		}

		StringBuilder builder = new StringBuilder(128)
		psg.each( (k,v) -> {
			Map m = v.countBy {it[1]}
			String hapCounts = m.collect( (h,c) -> { "${c}-${h}" } ).join(",")
			builder.append( ";${k.join(",")}:${hapCounts}" )
		})
		return "${psg.size()}${builder.toString()}"
	}

	/**
		* Returns a partial BED file line.
		*
		* It includes:
		* <ol>
		* <li>start
		* <li>end
		* <li>name: see below
		* <li>score: the total read count supporting the interval
		* </ol>
		*
		* The "name" field depends on whether it's a single- or multi-SNV
		* interval.
		*
		* Single-SNV intervals' name field contains (in order):
		* <ol>
		* <li>count of distinct haplotypes
		* <li>comma-separated haplotype counts
		* <li>comma-separated haplotype list
		* <li>delta between dominant and 2nd most abundant haplotype
		* </ol>
		*/
	public String toString() {
		String name = isSingleSNV() ? alleleSummary() : phaseSetSummary()
		return "${start-1}\t${end}\t${name}\t${this.depth}"
	}

	boolean sameSpan( Interval rhs ) {
		return this.start == rhs.start && this.end == rhs.end
	}

	/*boolean intersects( Interval rhs ) {
		return this.start < rhs.end && rhs.start < this.end
	}*/
}


class SnacOutput {

	/**
		* Read snac output and identify and segregate phaseable from
		* unphaseable SNVs.
		*/
	static List analyze( File input, int expected_read_count=100 ) {

		HashMap rids = new HashMap( expected_read_count )
		List records = []

		input.splitEachLine( /\t/, columns -> {

			if( columns[0].startsWith("#") ) {
				return
			}

			int na = Integer.parseInt( columns[2] )

			if( na > 4 ) {
				System.err.println( "skipping position ${columns[2]}: ${na} > 4 alleles" )
				return
			}

			// Reads supporting each alleles are listed in columns following
			// the alleles and their counts but in CORRESPONDING ORDER.

			(0 ..<na).each( allele_index -> {

				columns[ 3 + 2*na + allele_index ].split(",").each( read_id -> {
					rids.get( read_id, [] ) << [ records.size, allele_index ]
				})

			})

			records << new Interval( na, columns )

			// Print the original (without its reads) and reordered to confirm
			// correct reordering.

			if( Globals.DEBUG ) {
				//println "#RO\t${columns[ 0 ..<3+2*na].join(' ')} ... ${haplos.join(' ')} → ${reordered.join(' ')}"
			}
		})

		int reads_emitted = 0

		// Create additional (non-trivial) Intervals for reads that cover multiple SNVs.

		rids.each( (rdid, covered ) -> {

			if( covered.size > 1 ) {

				PhaseSet ps = new PhaseSet( reads_emitted )

				// Deduct the read's contribution to each SNV it supports.

				covered.each( ( rec_index, allele_index ) -> {
					ps << records[ rec_index ].remove( allele_index )
				})

				records << new Interval( ps )

				if( ! Globals.SKIP_QNAME_INDEX ) {
					println "RD\t${reads_emitted}\t${rdid}" // just slows things down in development
				}
				reads_emitted += 1
			}
		})

		records.sort( true ) // this sorts single- and multi-SNV intervals together.

		return records
	}

	/**
		* Collapse sorted reads
		*/
	static List mergePhaseSetsWithIdenticalSpans( List recs ) {

		List merged = []
		Interval anchor = null

		recs.each {

			if( it.isSingleSNV() ) {

				// single-SNV intervals are not merged, but they trigger flushes.

				if( anchor != null ) {
					merged << anchor
					anchor = null
				}

				merged << it

			} else
			if( anchor == null ) {
				anchor = it
			} else {
				if( it.sameSpan( anchor ) ) {
					anchor << it
				} else {
					merged << anchor
					anchor = it
				}
			}
		}

		if( anchor != null ) {
			merged << anchor
		}

		// TODO: merging unsorts things, but shouldn't. Resolve!
		//merged.sort( true )

		return merged
	}
}


////////////////////////////////////////////////////////////////////////////

// Tools that might be at strange PATHs should be handled by amending your
// PATH environment variable and placing their _basenames_ in this list.

// Each tool should be specified with a command line that will either:
// 1. provide its version information if any exists, or
// 2. if version is unavailable, simply insure it _runs_ and _exits_.
List REQUIRED_TOOLS = [
	// generic
	"bash -c exit",
	"tr --version",
	"awk --version",
	"grep --version",
	"paste --version",
	"md5sum --version",
	"Rscript --version",
	// bioinformatic
	"bedtools --version"
	]

/**
	* This class is just a namespace for miscellanous read-only globals.
	*/
class Globals {
	final static boolean DRYRUN  = System.getenv().DRYRUN   != null;
	final static boolean DEBUG   = System.getenv().DEBUG    != null;
	final static boolean VERBOSE = System.getenv().VERBOSE  != null;
	final static boolean SKIP_QNAME_INDEX	= System.getenv().SKIP_QNAME_INDEX != null;
	final static long MINUTES_TO_MS = 60*1000;
	final static long SECONDS_TO_MS =  1*1000;
}

// Verify environment contains required tools.

def DEV_NULL = new FileOutputStream( "/dev/null" )

try {

	for( tool in REQUIRED_TOOLS ) {
		// Following:
		// 1. insures each tool is executable
		// 2. provides version information for tools used on stdout
		// Each command line is executed twice, once piping stdout to this
		// process' stdout and a second time piping stderr to this process'
		// stdout because there is no fixed convention regarding to which
		// stream a command line tool should emit its version/help info.
		if( Globals.DRYRUN ) {
			println tool.split(/\s+/)[0]
		}

		Thread t1 = tool.execute().consumeProcessOutputStream(
			Globals.DRYRUN ? System.out : DEV_NULL )
		Thread t2 = tool.execute().consumeProcessErrorStream(
			Globals.DRYRUN ? System.out : DEV_NULL )

		t2.join( 10 * Globals.SECONDS_TO_MS )
		t1.join( 10 * Globals.SECONDS_TO_MS )

		if( Globals.DRYRUN ) {
			println "---"
		}
	}
}
catch( java.io.IOException ex) {
	println "One or more of ${REQUIRED_TOOLS} not on \$PATH"
	println ex
	System.exit(1)
}

DEV_NULL = null

// Arguments

// Much of following is just pre-run verifications that we have everything
// we need so that we fail _early_ if we're going to fail at all.

def help() {
	File scriptFile = new File( getClass().protectionDomain.codeSource.location.path )
	println "${scriptFile.getName() - ~/.groovy$/} <allele count>"
	System.exit(1)
}

if( args.size() < 1 || args[0] ==~ /-*h(elp)?/ ) { help() }

final File allele_count_file = new File( args[0] )

List input_hashes = []
for( f in [ allele_count_file ] ) {
	if( ! f.isFile() || Files.size( f.toPath() ) == 0 ) {
		println "${f.toString()} is missing or empty"
		System.exit(-1)
	} else {
		Process p = "md5sum ${f.toString()}".execute()
		input_hashes << p.getText()
	}
}

////////////////////////////////////////////////////////////////////////////
// Load

int decimated = 0
for( i in SnacOutput.mergePhaseSetsWithIdenticalSpans( SnacOutput.analyze( allele_count_file, 500000 ) ) ) {
	if( i.depth > 0 ) {
		println i
	} else {
		decimated += 1
	}
}


////////////////////////////////////////////////////////////////////////////
// Output


println "# ${decimated} DECIMATED"
input_hashes.each { print "# ${it}" }
println "# ${Instant.now().toString()}"
