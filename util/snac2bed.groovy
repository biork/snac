
/**
	* Merge either:
	* 1) all snac output files in a directory (multiple donors), or
	* 2) all snac output (contig) files associated with a single donor
	* ...into a single BED-like file.
	*
	* In both cases the file contains contig,start,end fields qualifying it as
	* "BED-like". The 4th column is the donor identifier, and the 5th contains
	* the counts of the two most abundant alleles output by snac formated as
	* "minor:major".
	*
	* In the first multiple-donor-file case the (contig,start) pair is encoded
	* into a single hexadecimal field for easy sorting of all donors by contig
	* and start position. This is intended to be used with other utilities,
	* for example:
	*
	* <pre>
	* groovy snac2bed.groovy counted \
	*		| sort -k 1,1 \
	*		| gpcodec -p chr \
	*		| tee counted-unmerged.bed \
	*		| bedtools merge -i - -c 3,4,5 -o count,collapse,collapse
	* </pre>
	*
	* Notes:
	* Snac's output filenames have been formatted by upstream scripts as
	* DONOR_CONTIG.tab, where "CONTIG" does <em>not</em> have a prefix.
	*/

import java.util.regex.Matcher;


/**
	* Convert (contig,start) to hexadecimal CCSSSSSSS where CC is contig
	* number in hexadecimal (with X→23, Y→24) and SSSSSSS is the start
	* coordinate in hexadecimal.
	* This format makes sorting by (contig,start) trivially easy using the
	* standard sort from core utilities.
	*/
String contigHex( String id, int pos ) {

	if( id ==~ /^[0-9]+$/ ) {
		return String.format( "%02X%07X", id as int, pos )
	} else
	if( id.equalsIgnoreCase("X") ) {
		return String.format( "17%07X", pos )
	} else
	if( id.equalsIgnoreCase("Y") ) {
		return String.format( "18%07X", pos )
	}
	throw RuntimeException("unexpected contig id:" + id )
}


/**
	* Reformat the output of the snac (allele-counting) utility to be
	* BED-like.
	* The snac tool handles one contig at a time so a single donor will
	* yield multiple output files, one for each chromosome typically
	* named like "counted/K014_22.tab".
	* Output from snac looks like:
	* ...
	* 50708212	4	2	3	G	1	A	ST-E00310:661:HT3CHCCXY:8:2118:28280:69994,ST-E00310:661:HT3CHCCXY:8:1223:7841:72842,ST-E00310:661:HT3CHCCXY:8:1119:9851:30439	ST-E00310:661:HT3CHCCXY:8:1116:28422:59622
	* 50708587	6	2	4	G	2	A	ST-E00310:661:HT3CHCCXY:8:1102:2970:2874,ST-E00310:661:HT3CHCCXY:8:1214:11454:56528,ST-E00310:661:HT3CHCCXY:8:2112:29112:48265,ST-E00310:661:HT3CHCCXY:8:2108:22820:6144	ST-E00310:661:HT3CHCCXY:8:1206:2027:29050,ST-E00310:661:HT3CHCCXY:8:1102:21197:72209
	* 50709996	4	2	2	A	2	G	ST-E00310:661:HT3CHCCXY:8:1209:29396:34025,ST-E00310:661:HT3CHCCXY:8:2102:16914:25306	ST-E00310:661:HT3CHCCXY:8:2120:9607:42921,ST-E00310:661:HT3CHCCXY:8:2209:7547:60800
	* ...
	*/
void reformatSnacFile( File path, String donor, String contig, boolean encode=false ) {
	path.withReader { rdr ->
		rdr.splitEachLine( /\t/, { fields ->
			if( ! fields[0].startsWith("#") ) { // skip comments
				int pos      = fields[0] as int
				int depth    = fields[1] as int // sum of _all_ allele counts
				int nalleles = fields[2] as int
				// Only the first 3 fields are guaranteed to be present.
				int major    = depth > 0 ? fields[3] as int : 0
				int minor = nalleles < 2 ? 0 : fields[5] as int
				assert minor <= major, "alleles assumed ordered descending by upstream processing"
				String name = String.format( "%d:%d", minor, major )
				List line_fields = encode ? \
						[ contigHex( contig, pos-1), pos, donor, name ] : \
						[ contig, pos-1,             pos, donor, name ]
				println line_fields.join('\t')
			}
		})
	}
}

////////////////////////////////////////////////////////////////////////////

ROOT = new File( args[0] )
PREFIX = args.length > 1 ? args[1] : ""

if( ROOT.isDirectory() ) {

	// Merge all files in directory

	ROOT.traverse( {
		String baseName = it.getName()
		if( baseName.endsWith(".tab") ) {
			Matcher m = baseName =~ /([A-Z]+[0-9]+)_([0-9XY]+).tab/
			if( m.find() ) {
				reformatSnacFile( it, m.group(1), m.group(2), true )
			}
		}
	})

} else 
if( ROOT.isFile() ) {

	// Merge all CONTIG files associated with the donor of the given snac
	// file in canonical contig order.
	// Followinng ASSUMEs the default snac output of abbreviated chromosome
	// identifiers, "9" and "X" rather than "chr9" and "chrX".

	List CONTIGS = ((1..22).collect({it.toString()})+["X","Y"])

	// Following regex should handle snac output basenames of
	// both K047_9.tab and 47_9.tab.
	Matcher m = ROOT.getName() =~ /([A-Z]*[0-9]+)_([0-9XY]+).tab/
	
	if( m.find() ) {
		String dirName = ROOT.getParent()
		String donor = m.group(1)
		CONTIGS.each {
			File snacFile = new File( dirName, String.format("%s_%s.tab", donor, it ) )
			if( snacFile.isFile() ) {
				reformatSnacFile( snacFile, donor, String.format( "%s%s", PREFIX, it ) )
			} else {
				System.err.print( "warning: no %s contig found in %s for %s\n", it, dirName, donor )
			}
		}
	}
} else {
	println "groovy snac2bed.groovy { <directory> | <snac output file> }"
}

