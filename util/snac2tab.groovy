
/**
	* Convert the ragged snac output to a table with the following
	* column..
	* 1. position
	* 2. num alleles
	* 3. comma-separated allele counts
	* 4. alleles
	* For downstream script convenience comma-separated counts will always
	* include at least two counts, where the second is 0 as necessary when
	* there is only 1 allele. In this case, the second allele will be "X".
	*/

if( ! split[0].startsWith('#') ) {

	def na = Integer.parseInt( split[2] )

	if( na > 0 ) {

		String ac = null
		String al = null

		if( na < 2 ) {
			ac = "${split[3]},0"
			al = "${split[4]}X"
		} else {
			ac = (0 ..<na).collect { split[3 + 2*it ] }.join( "," )
			al = (0 ..<na).collect { split[4 + 2*it ] }.join( "" )
		}

		println "${split[0]}\t${na}\t${ac}\t${al}"
	}
}

