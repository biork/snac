
/**
	* This script precisely undoes the "compression" carried out by
	* StripReadIds.groovy yielding the original file verbatim.
	*
	* Obviously, aside from being useful in its own right, the ability to
	* exactly reconstitute the original file also constitutes a test of
	* StripReadIds.groovy script.
	*
	* groovy StripReadIds.groovy SOME_SNAC_FILE /tmp/reads.txt &gt; /tmp/stripped.tab
	* groovy UnstripReadIds.groovy /tmp/reads.txt &lt; /tmp/stripped.tab | md5sum
	* md5sum SOME_SNAC_FILE
	*/

if( args.size() < 1 ) {
	println "groovy UnsriptReadIds.groovy read_ids < stripped_file.tab"
	System.exit(0)
}

List read_ids = (new File( args[0] )).readLines()

System.in.eachLine( line -> {

	if( line.startsWith('#') ) {

		println line

	} else {

		// 11418	60	3	35	T	24	C	1	G
		def fields = line.split('\t')
		def na = Integer.parseInt( fields[2] )
		def first_read_field = 3 + 2*na

		if( na == 0 ) {

			println line // It's a degenerate line, but leave it unscathed.

		} else {

			// Convert each column of comma-separated ordinals back into
			// the original read identifiers.

			String columns = (0..<na).collect( ai -> {
				fields[ first_read_field + ai ].split(',').collect { read_ids[Integer.parseInt(it,16)] }.join(",")
			}).join("\t")

			print "${fields[0]}\t${fields[1]}\t${fields[2]}"
			for(int i = 0; i < na; i++ ) {
				print "\t${fields[3+2*i]}\t${fields[4+2*i]}"
			}
			println "\t${columns}"
		}
	}
})

