
use std::env;
use std::io::{self,BufRead};
use std::collections::{BTreeMap,HashMap};
use std::str::FromStr;

use sam::{text::Record,Alignment};

pub const ENVVAR_EMIT_QNAME : &str = "EMIT_QNAME";
pub const ENVVAR_DEPTH : &str      = "SEQ_DEPTH";
pub const DEFAULT_DEPTH : usize    = 30;

/// A perfect hash function of ['A','C','G','T'].
/// A → 0
/// C → 1
/// G → 3
/// T → 2
///
/// ```rust
///	for c in vec!['A','C','G','T','N'] {
///		println!( "{}", base_hash( c ) );
/// }
/// ```
/// A	1000_00_1 65 0
/// C	1000_01_1 67 1
/// G	1000_11_1 71 3
/// T	1010_10_0 84 2
/// N	1001_11_0 78 3
fn base_hash( base : u8 ) -> usize {
	(((base as u16) >> 1) & 3) as usize
}

const HASH2BASE_CHAR : [char;4] = ['A',      'C',      'T',      'G'      ];
const HASH2BASE_U8   : [  u8;4] = ['A' as u8,'C' as u8,'T' as u8,'G' as u8];

const BA : u8 = 'A' as u8;
const BC : u8 = 'C' as u8;
const BG : u8 = 'G' as u8;
const BT : u8 = 'T' as u8;

/// Emit output for all positions up to but _excluding_ upper_limit, and
/// remove the relevant nodes from the map.
/// 12345678    36  2   20  A   16  G   ...
fn flush( evidence : &mut BTreeMap<u32,HashMap<String,u8>>, upper_limit : u32, emit_qname : bool ) -> io::Result<()> {

    assert!( evidence.len() > 0 );

    let mut removeable = Vec::<u32>::with_capacity(8);

    // TODO: Remainder of this function would be more efficiently
    // implemented using drain_filter if it moved out of nightly.
    
    for pos in evidence.keys() {
        if *pos < upper_limit {
    
            removeable.push( *pos );

            let h = evidence.get(&pos).expect("verified above");

            // Count reads associated with each base in the
            // HashMap h:read_id -> base

            let mut counts : Vec<(i32,usize)> = vec![(0_i32,0),(0_i32,1),(0_i32,2),(0_i32,3)];
            //                                              A         C         T         G

            for (_,v) in h {
                assert!( *v == BA || *v == BC || *v == BG || *v == BT );
                counts[ base_hash( *v ) ].0 += 1;
            }
       
            let total_depth : i32 = counts.iter().map(|(c,_)| *c).sum();
            let num_alleles : u32 = counts.iter().map(|(c,_)| if *c > 0 {1} else {0} ).sum();

            print!( "{}\t{}\t{}", pos, total_depth, num_alleles );

            // Find the permutation that orders bases' counts descending,
            // and emit counts of reads covering alleles.

            counts.sort_by_key( |(c,_)| -(*c) );

            let mut i : usize = 0;

            while i < 4 && counts[i].0 > 0 {
                print!( "\t{}\t{}", counts[i].0, HASH2BASE_CHAR[ counts[i].1 ] );
                i += 1
            }

            // Multiple iteractions seems more efficient that creatig a
            // bunch of short-lived datastructs to accomplish the sorted
            // output directly(?)

            if emit_qname {

                i = 0;
                while i < 4 && counts[i].0 > 0 {
                    let base : u8 = HASH2BASE_U8[ counts[i].1 ];
                    let mut sep = '\t';
                    for (qname,_) in h.iter().filter( |(_,v)| **v == base ) {
                        print!( "{}{}", sep, qname );
                        sep = ','
                    }
                    i += 1
                }
            }
            print!("\n");
        }
    }

    for pos in removeable {
        evidence.remove(&pos);
    }

    Ok(())
}


/// Scan SAM-format alignments assumed to be:
/// 1. ordered by coordinates and
/// 2. confined to a single contig.
pub fn scan_alignments(
	stream : &mut dyn BufRead,
	position : &Vec<u32>,
	max_tlen : i32 ) -> io::Result<(usize,usize,usize,usize,usize)> {

	let emit_qname = env::var(ENVVAR_EMIT_QNAME).is_ok();
	let expected_depth : usize = match env::var(ENVVAR_DEPTH) {
			Ok(val) => val.parse().unwrap(),
			Err(_e) => DEFAULT_DEPTH
	};

	let n = position.len();

	let mut covered : BTreeMap<u32,HashMap<String,u8>> = BTreeMap::new();
	let mut head : usize;
	let mut tail : usize = 0;

	// Following three counts are returned by this function and become
	// a comment footer in the output.
	let mut scanned : usize = 0;
	let mut skipped_len : usize = 0;
    let mut skipped_n : usize = 0;
	let mut overlap : usize = 0;
	let mut discord : usize = 0;

	let mut last_rd_min  = 0; // strictly for verifying read ordering.

	// Set up to iterate SAM lines that are expected to have been
	// pre-filtered and sorted by contig coordinate (POS).

	let records = stream.lines()
		.map( |res| Record::from_str( &res.unwrap() ).unwrap() );

	for rd in records { // For every alignment that...

		// ...passes upstream filters and is valid according to the following...

		if rd.tlen.abs() > max_tlen {
			eprintln!( "TLEN\t{}\t{}", rd.qname, rd.tlen );
			skipped_len += 1;
			continue;
		}

		scanned += 1;

		let rd_min_pos : u32 = rd.pos;
		let rd_max_pos : u32 = rd.endpos();

		// Any positions less than rd_min_pos are complete (assuming
		// reads are coordinate-ordered as expected!)

        if ! covered.is_empty() {
		    flush( &mut covered, rd_min_pos, emit_qname )?;
        }

		assert!( last_rd_min <= rd_min_pos,
			"out-of-order read: {}@{} < {}(the last read position)",
			rd.qname, rd_min_pos, last_rd_min );
		last_rd_min = rd_min_pos;

		// Find the first SNV that _could_ be covered by the current read.

		while tail < n && position[ tail ] < rd_min_pos {
			tail += 1;
		}

		// Identify all SNVs covered by the current read.

		head = tail;
		while head < n && position[ head ] <= rd_max_pos {

			let cur_pos = position[ head ];

			if let Ok(base) = rd.base_at_ref_position( cur_pos ) {

                if base == ('N' as u8) {
                    skipped_n += 1;
			        head += 1;
                    continue;
                }

				let h = covered.entry( cur_pos ).or_insert( HashMap::with_capacity(expected_depth) );

				// Insure _overlapping_ alignments from the same template are not
				// counted redundantly.

				if let Some(mate_base) = h.get( &rd.qname ) {

                    // Overlap exists between what are presumably paired-end
                    // reads; do the two reads agree on the base?
                    
                    if *mate_base == base {

                        overlap += 1; // Yes! Just avoid double counting.

                    } else {

                        // Earlier-stored base does not agree with the
                        // current, so yank the earlier and discard the
                        // current.

                        discord += 1;
                        h.remove( &rd.qname );
                    }
					
				} else {

					h.insert( rd.qname.clone(), base );
				}
			}
			head += 1;
		}
	}

	if ! covered.is_empty() {
		flush( &mut covered, position.last().unwrap()+1 /* 1bp past last SNV position */, emit_qname )?;
	}

	Ok((scanned,skipped_len,skipped_n,overlap,discord))
}


#[cfg(test)]
mod tests {
	use super::*;
	use std::io::Cursor;

	const TWO_READS : &'static str = "\
ZR-G10333:155:FSF931AZH:4:5341:16336:32417	99	18	22338974	60	150M	=	22339076	252	TGGATGGGCTTAATAATAGAGTAATAGAATAGAGATAACAGAGAACAAAACCAACGATCTGGAGGACATAACAATAGAATTCCCCTAATCTGAACAAGAGAGAAAATAGACTGAAAACAGAAGTGGACACAGCCTCTGTGATCTGTGGTA	AAFFFJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJFJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJ	NM:i:0	MD:Z:150	AS:i:150	XS:i:0	RG:Z:4
ZR-G10333:155:FSF931AZH:4:5341:16336:32417	147	18	22339076	60	150M	=	22338974	-252	AAAATAGACTGAAAACAGAAGTGGACACAGCCTCTGTGATCTGTGGTAAAGTAAGCAACAGAGCCAGGTTGATATCATCACAATCCCAGAGAAAAGGAGAAAAAGAGTGGGGCTAGAAGAGCATTTGAAGAAATTATGGTTGAGAGCTTC	JJJJFAJJAAJJFF<FA-JJJJJJAAJJJJFJJFJFAAFJJFFFJJAJJJJFJJJJJJJFFJJAJJ<JJJJJJFA<F-JJJJJJJJJJAJJJJJJFJ<JFF7JJJJJJJFJJJFJJJJJJJFJJJJJJJJJJJJJJJJJJJJJFJFF-AA	NM:i:0	MD:Z:150	AS:i:150	XS:i:0	RG:Z:4";

	#[test]
	fn overlaps() {
		let snv_positions = [ 22339076, ].to_vec();
		if let Ok(counts) = scan_alignments( &mut Cursor::new( TWO_READS ), &snv_positions, 1000 ) {
			assert_eq!( counts.2, 1 );
		}
	}
}

