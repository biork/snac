// vim: noet:sw=2:ts=2:sts=2

//! Find a BED interval cover of the sequence of SNVs in snac results
//! such that SNVs within each output interval are fully linked by
//! reads. Alternatively, no read exists linking two SNVs in different
//! intervals.
//!
//! This is really just a way to decompose snac output into smaller-than-
//! chromosome-sized "chunks" for downstream processing. In particular,
//! this code entirely ignores locus structure; it is purely a result of
//! read mapping.

use std::io::{self,BufRead};
use std::str::FromStr;
use std::env;

/// Alias just to clarify the role these unsigned integers serve.
type ReadId = usize;

struct Bounds(u32,u32); // Fully closed; both are 1-based positions.

/*struct Allele {
	pub _depth : u16,
	pub _allele : u8,
}*/


struct SnvPileup {

	pub position: u32,
	pub total_depth: usize,
/*
	pub allele_count: u8,
	pub allele_depths: Vec<Allele>,
*/
	/// Each allele is supported by a Vec<ReadId>.
	pub support: Vec<Vec<ReadId>>
}


#[derive(Debug)]
pub enum SnacFromStringError {
	Parse( std::num::ParseIntError ),
	Failure,
}

impl From<std::num::ParseIntError> for SnacFromStringError {
	fn from( err:std::num::ParseIntError ) -> Self {
		SnacFromStringError::Parse(err)
	}
}

impl FromStr for SnvPileup {

	type Err = SnacFromStringError;

	fn from_str( s: &str ) -> Result<Self,Self::Err> {

		let fields:Vec<&str> = s.trim().split('\t').collect();

		// Parse the fixed-length fields.
		
		// Example line:
		//     0	 1	2	 3	4	 5	6	             7	       8
		// 16288	21	2	11	C	10	G	22,26,27,28,29	2e,2f,30
		let position : u32	= fields[0].parse()?;
		let total_depth : usize = fields[1].parse()?;
		let allele_count : u8 = fields[2].parse()?;

		assert!( allele_count > 0 || total_depth == 0 );

		// Parse the variable-length fields.
		//let mut allele_depths : Vec<Allele> = Vec::<Allele>::with_capacity(4);
		let mut support : Vec<Vec<ReadId>> = Vec::<Vec<ReadId>>::with_capacity(4);
		
		for i in 0..allele_count {
/*
			allele_depths.push( Allele {
				depth: fields[ 3+2*i as usize ].parse::<u16>()?,
				allele: fields[ 4+2*i as usize ].as_bytes()[0] } );
*/
			// Each allele's support is a Vec<ReadId>.
			support.push(
				fields[ (3+2*allele_count + i) as usize ].split(',')
				.map( |s| ReadId::from_str_radix(s,16).unwrap() ).collect() );
		}
		Ok( SnvPileup { position, total_depth, /*allele_count, allele_depths,*/ support })
	}
}


fn main() -> io::Result<()> {

	let verbose : i32 = match env::var( "VERBOSE" ) {
			Ok(val) => val.parse().unwrap(),
			Err(_e) => 0 
	};

	let args : Vec<String> = env::args().skip(1).collect();
	let seqname : &str = if args.len() > 0 { &args[0] } else {"sequence"};
	let stream = io::BufReader::new( io::stdin() ); // File::open(&args[0])?
	let snv_pileups = stream.lines()
		.filter( |l| ! l.as_ref().unwrap().starts_with( "#" ) )
		.map( |res| SnvPileup::from_str( &res.unwrap() ).unwrap() );

	let mut max_span : u32 = 0;
	let mut max_span_index : usize = 0;

	// The endpoints of each span in spans are SNVs covered by the same read.
	// ReadIds are the indices of spans.
	//
	// This works because snac/util/StripReadIds.groovy converts the long
	// (e.g. Illumina) read identifiers to integers _sequentially_ in the
	// order they are encountered in the file.

	let mut spans = Vec::<Bounds>::with_capacity( 500_000 );

	for (_i, pileup) in snv_pileups.enumerate() {

		assert!( pileup.position >= 1, "input is expected to be 1-based" );

		if pileup.total_depth > 0 {
			for allele_support in pileup.support {
				for read_index in allele_support {

					if read_index >= spans.len() {

						// The 1st encounter with a read index establishes
						// it's lower _and_ upper bounds.
						
						assert!( read_index == spans.len(),
							"Read id stripper must insure reads are numbered in order encountered." );
						spans.push( Bounds( pileup.position, pileup.position ) );

					} else {

						// Subsequent encounters raise its upper bound.
						
						assert!( spans[ read_index ].1 < pileup.position,
							"spans' upper bounds should increase monotonically." );

						spans[ read_index ].1 = pileup.position;

						// Following is only for purpose of identifying the read with
						// the greatest span as a sanity check for downstream processes.

						let span = spans[ read_index ].1 - spans[ read_index ].0 + 1;
						if max_span < span {
							max_span = span; 
							max_span_index = read_index;
						}
					} // if read_index >= spans.len()

				} // for read_index in allele_support
			} // for allele_support in pileup.support
		} // if pileup.total_depth > 0
	}

	// Print spans of individual reads.
/*	
	for (i,b) in spans.iter().enumerate() {
		print!( "RS\t{}\t{}\t{}\n", i, b.0, b.1 );
	}
*/

	// Print the minimal disjoint cover of the chromosome
	// as BED intervals (0-based, half-open)

	let mut lower : u32 = 0;
	let mut upper : u32 = 0;
	let mut last  : u32 = 0;

	for (_i,b) in spans.iter().enumerate() {

		assert!( last <= b.0, "spans should be ordered by lower bounds" );

		if b.0 <= upper {
			upper = upper.max(b.1);
		} else {
			if lower > 0 {
				print!( "{}\t{}\t{}\n", seqname, lower-1, upper );
			}
			lower = b.0;
			upper = b.1;
		}

		last = b.0;
	}

	// There may, in fact, be no reads...empty file.
	
	if lower > 0 {

		print!( "{}\t{}\t{}\n", seqname, lower-1, upper );

		if verbose != 0 {

			// An RNAseq read with too great a span is likely mis-mapped or
			// chimeric.

			print!( "# max read span [{},{}] is {}bp at index {}\n",
				spans[ max_span_index ].0,
				spans[ max_span_index ].1, max_span, max_span_index );
		}
	} else 
	if verbose != 0 {
		eprint!( "no reads\n" );
	}

	Ok(())
}

