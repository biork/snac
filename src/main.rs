
//! This program counts _alignments_ covering a specified set of positions
//! as well as the alleles observed at those positions.
//!
//! ## Input requirements.
//!
//! 1. A list of SNV positions must be provided either:
//!
//!		a) in a file containing a _single-column_ of SNV positions, or
//!		b) a _filtered_ VCF file containing _nothing but SNVs_.
//!
//! 2. _prefiltered_ SAM-format read data (which can be generated
//!		just-in-time from BAMs on the command line by process
//!		substitution).
//!
//! # Output format
//!
//! The output is line-oriented, one line per position.
//! The lines have variable field counts, so it is _not_ a proper table.
//!
//! ```
//! 12345678    36  2   20  A   16  G   ST-E00142:748:HLKTNCCXY:4:1117:28483:45241,...  102 ST-E00317:669:HT3T3CCXY:8:2121:16447:70486,...
//! ```
//! A line contains in order:
//! 1. coordinate (1-based)
//! 2. total read depth
//! 3. number of observed alleles
//! 4. (count,allele) tab-separated pairs in _descending_ order of abundance
//! 5. the comma-separated read identifiers supporting each allele in same order
//!     as the preceding alleles.
//!

use std::env;
use std::fs::File;
use std::io::{self,BufRead,BufReader};

use snac::DEFAULT_DEPTH;
use snac::ENVVAR_EMIT_QNAME;
use snac::ENVVAR_DEPTH;

const ENVVAR_MAX_TLEN    : &str = "MAX_TLEN";
const ENVVAR_EMIT_HEADER : &str = "EMIT_HEADER";

const GIT_COMMIT : &str = env!("SNAC_COMMIT","export SNAC_COMMIT=$(git rev-parse HEAD | cut -b 1-8) before compiling.");


/// Load a sequence of unsigned integers from a file containing exactly one
/// numeric column and nothing else.
fn load_u32_sequence( path: &str ) -> io::Result<Vec<u32>> {

	let rdr = BufReader::new( File::open( path )? );
	let mut v : Vec<u32> = rdr.lines().map( |line| line.unwrap().parse().unwrap() ).collect();
	v.sort_unstable();
	Ok(v)
}


/// Pull just the POS column out of the data portion of a VCF.
///
/// This program is typically run on position lists derived from VCF files.
/// This function is a small convenience for scripts to avoid extracting
/// position lists from VCF files, but VCF files still must be filtered!
///
/// This function is obviously not even the beginnings of a general-purpose
/// VCF parser. It is equivalent to:
///
///    'bcftools query -f "%POS\n" ...'
///
/// ...implying that it does _no filtering whatsoever_ on the VCF file.
/// Clearly it makes no sense to return all positions unless the VCF has
/// at least already been filtered to contain a single contig!

fn load_u32_sequence_from_vcf( path: &str ) -> io::Result<Vec<u32>> {

	let rdr = BufReader::new( File::open( path )? );
	let mut v : Vec<u32> = rdr.lines()
		.filter( |line| ! line.as_ref().unwrap().starts_with( "#" ) )
		.map( |line| {
				let l = line.unwrap();
				let mut split_iter = l.split('\t');
				split_iter.nth(1).unwrap().parse().unwrap()
		} ).collect();
	v.sort_unstable();
	Ok(v)
}


fn print_help( executable : &str ) {
	println!( r#"{} <SNV positions> <SAM file>

<SNV positions> may be:
1. a plain text file containing 1 column of _sorted_ 1-based positions
   for one chromosome, or
2. a sorted fragment of VCF file containing one chromosome.

_All_ positions are used, so if <SNV positions> is a VCF file, 
pre-filtered to contain:
* only one chromosome and,
* only positions of interest on that chromosome.

You can optionally define 4 environment variables.
1. {}=<integer> filters alignments of templates longer than <integer>.
2. {}='true' includes template identifiers (QNAMEs) in output. Note
   that this dramatically bloats the size of the output but facilitates
   evidence tracing downstream.
3. {}='true' includes a header. Output lines contain varying field
   counts, so this header will not account for all possible columns.
4. {}=<integer> optimizes list creation for the expected coverage depth.
   Defaults to {}.

<SAM file> must be a SAM--that is, _text_ file--not a BAM.

Environment variables can be defined on the command line. Note also
that you can use "process substitution" (in the bash shell) in place
of either positional argument. For example:

    {}=1234 {} <(bcftools view -r chrX some.vcf) <(samtools view -q 20 -f 0x3 -F 0xF0C some.bam chrX)

git commit: {}
"#, executable,
    ENVVAR_MAX_TLEN,
    ENVVAR_EMIT_QNAME,
		ENVVAR_EMIT_HEADER,
		ENVVAR_DEPTH, DEFAULT_DEPTH,
    ENVVAR_MAX_TLEN, executable,
	GIT_COMMIT );
}


fn main() -> io::Result<()> {

	let max_tlen : i32 = match env::var(ENVVAR_MAX_TLEN) {
			Ok(val) => val.parse().unwrap(),
			Err(_e) => i32::MAX
	};

	if env::args().count() < 2 || env::args().any( |arg| arg == "help" ) {
		print_help( &env::args().next().unwrap() );
		return Ok(());
	}

	let args: Vec<String> = env::args().skip(1).collect();

	let snv_positions : Vec<u32> = if args[0].ends_with("vcf") {
		load_u32_sequence_from_vcf( &args[0] )?
	} else {
		load_u32_sequence( &args[0] )?
	};

    // This is not an error. There merely won't be any output to report,
    // so give some indication why...
	if snv_positions.len() < 1 {
			println!( "# WARNING\tSNV positions input was empty." );
	}

	let mut stream = io::BufReader::new( File::open(&args[1])? );

	// The header is really a comment; it can't be treated as a true header
	// because lines have variable field counts--that is, the output is not
	// a "proper" table. For this reason, the header is #-prefixed so most
	// scripts can ignore it as the comment it really is.

	if env::var(ENVVAR_EMIT_HEADER).is_ok() {
		println!("#position\tdepth\tnum_alleles\tallele_1_depth\tallele_1_base\tallele_2_depth...");
	}

	if let Ok(counts) = snac::scan_alignments( &mut stream, &snv_positions, max_tlen ) {
			println!( "# SCANNED\t{}", counts.0 );
			println!( "# SKIPPED-LENGTH\t{}", counts.1 );
			println!( "# SKIPPED-N\t{}", counts.2 );
			println!( "# OVERLAP\t{}", counts.3 );
			println!( "# DISCORD\t{}", counts.4 );
			println!( "# MAX_TLEN\t{}", max_tlen );
			println!( "# GIT-COMMIT\t{}", GIT_COMMIT );
	}

	Ok(())
}

