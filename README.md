# Single Nucleotide Allele Counting

This program was developed in the lab of [Merja Heinäniemi](https://uefconnect.uef.fi/en/person/merja.heinaniemi/)
at the [University of Eastern Finland](https://www.uef.fi).

# Building

You will need:
1. a recent Rust toolchain and
2. the companion sam package (which will be downloaded automatically by `cargo build`).

# Usage

<tt>
./target/debug/snac \<SNV positions\> \<SAM file\>

\<SNV positions\> may be:
1. a plain text file containing 1 column of _sorted_ 1-based positions
   for one chromosome, or
2. a sorted fragment of VCF file containing one chromosome.

_All_ positions are used, so if \<SNV positions\> is a VCF file, 
pre-filtered to contain:
* only one chromosome and,
* only positions of interest on that chromosome.

You can optionally define 4 environment variables.
1. MAX_TLEN=\<integer\> filters alignments of templates longer than \<integer\>.
2. EMIT_QNAME='true' includes template identifiers (QNAMEs) in output. Note
   that this dramatically bloats the size of the output but facilitates
   evidence tracing downstream.
3. EMIT_HEADER='true' includes a header. Output lines contain varying field
   counts, so this header will not account for all possible columns.
4. SEQ_DEPTH=\<integer\> optimizes list creation for the expected coverage depth.
   Defaults to 30.

\<SAM file\> must be a SAM--that is, _text_ file--not a BAM.

Environment variables can be defined on the command line. Note also
that you can use "process substitution" (in the bash shell) in place
of either positional argument. For example:

    MAX_TLEN=1234 ./target/debug/snac <(bcftools view -r chrX some.vcf) <(samtools view -q 20 -f 0x3 -F 0xF0C some.bam chrX)

git commit: 71d92d8b
</tt>

