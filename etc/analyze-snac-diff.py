# vim: noet:sw=2:ts=2:sts=2

"""
This script analyzes the difference in output between snac versions before
and after 5a679c8c to insure only expected changes occurred in counts.
"""

from sys import stdin,exit
from re import match as rx_match

_ALLELES = ('A','C','G','T','N')

class SnacRec:
	#< 60874	39	3	33	C	5	T	1	G
	#---
	#> 60874	37	2	32	C	5	T
	def __init__( self, fields ):
		self.pos   = int(fields[0])
		self.depth = int(fields[1])
		self.alleles = [ ( int(fields[3+2*i]), fields[4+2*i] ) for i in range(0,int(fields[2])) ]
		assert all( a[1].isupper() for a in self.alleles )

	def __len__( self ):
		return len(self.alleles)

	def count( self, allele ):
		assert allele.isupper()
		for c,a in self.alleles:
			if a == allele:
				return c
		return 0

	@staticmethod
	def reconcile( o, n ):
		"""
		Confirm total read reduction is accounted for by the lower allele
		counts.
		"""
		nreads = o.depth - n.depth
		# Some positions will show up as diffs because of unstable sorting
		# of alleles with equal counts even though read count did not change,
		if nreads == 0:
			return
		# but...
		if not ( n.depth < o.depth ):
			print( "error: reads should never be added." )

		allele_diffs = [
			o.count(a) - n.count(a) for a in _ALLELES ]

		if not (len(n) <= len(o)):
			print( "error: number of distinct alleles should only decrease" )
		if not (sum(allele_diffs) == nreads):
			print( "error: sum of allele count discrep should be same as read count discrep" )

		print( o.pos, nreads, *allele_diffs, len(o)-len(n), sep="\t" )
		

old = {}
new = {}

while True:
	line = stdin.readline()
	if len(line) < 1:
		break
	if rx_match(r'^[<>]', line ):
		fields = line[2:].rstrip().split('\t')
		(old if line[0] == "<" else new)[ int(fields[0]) ] = SnacRec( fields )

for (k,o) in old.items():
	try:
		SnacRec.reconcile( o, new[k] )
	except KeyError:
		print( f"error: new results do not have record for position {k}" )

