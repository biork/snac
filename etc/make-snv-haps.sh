
HETSNV_LOCATIONS=${1:-'/home/raj/uef/K023/allele_counts/DNAseq_somatic_9p.tab'}
FASTA_HAPLOTYPE_TEMPLATE=${2:-'/home/raj/uef/K023/haplotypes/ALLT-323_R.%d.fa'}
FASTA_CONTIG_PREFIX=${3:-'chr9p'}
CONTIG_SIZE=42000000

# Pull the haplotypes at a set of hetSNV positions out of a pair of FASTA
# files giving the (putative) full haplotypes of a contig.

bedtools getfasta \
	-fi $(printf ${FASTA_HAPLOTYPE_TEMPLATE} 1) \
	-bed <(awk -v OFS=$'\t' "!/^#/ && \$1 < ${CONTIG_SIZE} {print \"${FASTA_CONTIG_PREFIX}1\",\$1-1,\$1}" ${HETSNV_LOCATIONS}) \
	| grep -v '^>' > .1
bedtools getfasta \
	-fi $(printf ${FASTA_HAPLOTYPE_TEMPLATE} 2) \
	-bed <(awk -v OFS=$'\t' "!/^#/ && \$1 < ${CONTIG_SIZE} {print \"${FASTA_CONTIG_PREFIX}2\",\$1-1,\$1}" ${HETSNV_LOCATIONS}) \
	| grep -v '^>' > .2

paste <(awk -v OFS=$'\t' "!/^#/ && \$1 < ${CONTIG_SIZE} {print \$1}" ${HETSNV_LOCATIONS}) .1 .2 | tr '[:lower:]' '[:upper:]'
